package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    public double getBookPrice(String temperature){
        double fahrenheit = (Double.parseDouble(temperature) * 9/5) + 32;
        return fahrenheit;
    }
}
